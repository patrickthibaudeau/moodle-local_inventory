define(['jquery', 'jqueryui', 'local_inventory/datatable'], function ($, jqui, DataTable) {
    "use strict";

    /**
     * This is the function that is loaded
     * when the page is viewed.
     * @returns {undefined}
     */
    function initAreasPage() {
        initAreas();
    }




    /**
     * Holds all functionality for the user select box
     * @returns {undefined}
     */
    function initAreas() {
        var wwwroot = M.cfg.wwwroot;

        $('#areasTable').DataTable();

        $('.deleteArea').click(function () {
            var id = $(this).data('id');
            if (confirm('Delete area?')) {
                $.ajax({
                    type: 'GET',
                    url: "ajax.php?action=deleteArea&id=" + id,
                    dataType: 'html',
                    success: function (resultData) {
                        console.log(resultData);
                        $('#results-container').html(resultData);
                        initAddRemoveStock();
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });

        $('#btn-search-sku').click(function () {
            var wwwroot = M.cfg.wwwroot;
            var id = $('#sku-search').val();
            $(location).attr('href', wwwroot + '/local/inventory/index.php?id=' + id);
        });
    }




    return {
        init: function () {
            initAreasPage();
        }
    };
});