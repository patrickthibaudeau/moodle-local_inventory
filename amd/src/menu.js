define(['jquery', 'jqueryui', 'local_inventory/select2'], function ($, jqui, select2) {
    "use strict";

    /**
     * This is the function that is loaded
     * when the page is viewed.
     * @returns {undefined}
     */
    function initMenuPage() {
        initSkuSearchBox();
    }




    /**
     * Holds all functionality for the user select box
     * @returns {undefined}
     */
    function initSkuSearchBox() {
        var wwwroot = M.cfg.wwwroot;

        $("#sku-search").select2({
            placeholder: 'Select',
            allowClear: true,
            width: 250,
            ajax: {
                url: wwwroot + "/local/inventory/ajax.php?action=searchSku",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // Search term
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // Let our custom formatter work
            minimumInputLength: 2,
            multiple: false
        });

        $('#sku-search').on('select2:select', function (e) {

            var id = $('#sku-search').val();
            $.ajax({
                type: 'GET',
                url: "ajax.php?action=getItem&id=" + id,
                dataType: 'html',
                success: function (resultData) {
                    console.log(resultData);
                    $('#results-container').html(resultData);
                    initAddRemoveStock();
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('#btn-search-sku').click(function () {
            var wwwroot = M.cfg.wwwroot;
            var id = $('#sku-search').val();
            $(location).attr('href', wwwroot + '/local/inventory/index.php?id=' + id);
        });
    }

    function initAddRemoveStock() {
        $('.addStock').click(function () {
            var id = $(this).data('itemshelfid');
            var amount = $(this).data('amount');
            var action = 'addStock';
            adjustStockLevel(id, amount, action);

        });
        $('.removeStock').click(function () {
            var id = $(this).data('itemshelfid');
            var amount = $(this).data('amount');
            var action = 'removeStock';
            adjustStockLevel(id, amount, action);
        });
    }

    function adjustStockLevel(id, amount, action) {
        var wwwroot = M.cfg.wwwroot;
        $.ajax({
            type: 'GET',
            url: "ajax.php?action=" + action + "&id=" + id + "&amount=" + amount,
            dataType: 'html',
            success: function (resultData) {
                console.log(resultData);
                $('#results-container').html(resultData);
                initAddRemoveStock();
            },
            error: function (e) {
                console.log(e);
            }
        });
    }


    return {
        init: function () {
            initMenuPage();
        }
    };
});