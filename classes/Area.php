<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory;

/**
 * Description of Area
 *
 * @author patrick
 */
class Area {
  
    /**
     *
     * @var int 
     */
    private $id;
  
    /**
     *
     * @var int 
     */
    private $store_id;
  
    /**
     *
     * @var string 
     */
    private $name;
  
    /**
     *
     * @var int 
     */
    private $time_created;
  
    /**
     *
     * @var int 
     */
    private $time_modified;
  
    /**
     *
     * @var string 
     */
    private $table;
    
    public function __construct($id) {
        global $CFG, $DB, $USER;
        
        if ($id) {
            $area = $DB->get_record($this->table, ['id' => $id]);
            
            $this->id = $id;
            $this->name = $area->name;
            $this->time_created = date('m-d-Y H:i:s',$area->timecreated);
            $this->time_modified = date('m-d-Y H:i:s',$area->timemodified);
        }
    }
    
    public function get_id() {
        return $this->id;
    }

    public function get_store_id() {
        return $this->store_id;
    }

    public function get_name() {
        return $this->name;
    }

    public function get_time_created() {
        return $this->time_created;
    }

    public function get_time_modified() {
        return $this->time_modified;
    }

    public function get_table() {
        return $this->table;
    }


    
   
}
