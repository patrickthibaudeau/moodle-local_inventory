<?php

/**
 * *************************************************************************
 * *                             inventory                                **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  inventory                                                 **
 * @name        inventory                                                 **
 * @copyright   Dominik Thibaudeau                                        **
 * @link                                                                  **
 * @author      Dominik Thibaudeau                                        **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

namespace local_inventory;

class Base {

    /**
     * Creates the Moodle page header
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \moodle_page $PAGE
     * @global \stdClass $SITE
     * @param string $url Current page url
     * @param string $pagetitle  Page title
     * @param string $pageheading Page heading (Note hard coded to site fullname)
     * @param array $context The page context (SYSTEM, COURSE, MODULE etc)
     * @return HTML Contains page information and loads all Javascript and CSS
     */
    public static function page($url, $pagetitle, $pageheading, $context, $pagelayout = 'admin') {
        global $CFG, $PAGE, $SITE;

        $stringman = get_string_manager();
        $strings = $stringman->load_component_strings('local_inventory', current_language());

        $PAGE->set_url($url);
        $PAGE->set_title($pagetitle);
        $PAGE->set_heading($pageheading);
        $PAGE->set_pagelayout('base');
        $PAGE->set_context($context);
        $PAGE->requires->css('/local/inventory/js/datatable_1_10_18/DataTables-1.10.18/css/jquery.dataTables.min.css');
        $PAGE->requires->css('/local/inventory/js/datatable_1_10_18/Buttons-1.5.2/css/buttons.dataTables.min.css');
        $PAGE->requires->css('/local/inventory/js/daterangepicker/daterangepicker.css');
        $PAGE->requires->css('/local/inventory/js/select2-4.0.3/dist/css/select2.min.css');
        $PAGE->requires->css('/local/inventory/css/jquery-ui.min.css');
        $PAGE->requires->css('/local/inventory/css/forms.css');
        $PAGE->requires->strings_for_js(array_keys($strings), 'local_inventory');
    }

    /**
     * Sets filemanager options
     * @global \stdClass $CFG
     * @param \stdClass $context
     * @param int $maxfiles
     * @return array
     */
    public static function getFileManagerOptions($context, $maxfiles = 1) {
        global $CFG;
        return array('subdirs' => 0, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => $maxfiles);
    }

    /**
     * This function provides the javascript console.log function to print out php data to the console for debugging.
     * @param string $object
     */
    public static function consoleLog($object) {
        $html = '<script>';
        $html .= 'console.log("' . $object . '")';
        $html .= '</script>';

        echo $html;
    }

    public static function getEditorOptions($context) {
        global $CFG;
        return array('subdirs' => 1, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => -1,
            'changeformat' => 1, 'context' => $context, 'noclean' => 1, 'trusttext' => 0);
    }

    /**
     * Search users based on id, firstname or lastname. Used for jquery select2
     * @global stdobject $CFG
     * @global moodle_database $DB
     * @global session $USER
     * @param string $term Parameter is captured through URL
     * @return array $user JSON encoded array to be used with javascript
     */
    public static function searchSku() {
        global $CFG, $DB, $USER;

        $term = optional_param('q', '', PARAM_RAW);
        $sql = "SELECT id,name,sku FROM {local_inventory_item} WHERE ";
        $sql .= "sku LIKE '%$term%' ";
        $sql .= " ORDER BY name";

        $results = $DB->get_records_sql($sql);
        foreach ($results as $result) {
            $skus[] = array('id' => $result->id, 'text' => $result->name . ' (' . $result->sku . ')');
        }
        return json_encode($skus);
    }

    /**
     * Returns a single user record based on
     * 1. user id
     * 2. user name
     * 3. id number
     * $id will always have precedence
     * If an id is provide as well as a username, id will be used.
     * if an id is provided as well as a username and idnumber, id will be used
     * if a username and an id number are provided, username will be used.
     * @global \moodle_database $DB
     * @param int $id
     * @param string $username
     * @param string $idnumber
     */
    public static function getUserRecord($id = 0, $userName = '', $idNumber = '') {
        global $DB;


        if ($id != 0) {
            return $DB->get_record('user', array('id' => $id));
        }

        if ($userName != '') {
            return $DB->get_record('user', array('username' => $userName));
        }

        if ($idNumber != '') {
            return $DB->get_record('user', array('idnumber' => $idNumber));
        }
    }

}
