<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory;

/**
 * Description of Stores
 *
 * @author patrick
 */
class Stores {
    
     private $stores;
    
    /**
     * 
     * @global \stdCLass $CFG
     * @global \moodle_database $DB
     */
    public function __construct() {
        global $CFG, $DB;
        
        $this->stores = $DB->get_records('local_inventory_store',[]);
        
    }
    
    public function get_stores() {
        return $this->stores;
    }


}
