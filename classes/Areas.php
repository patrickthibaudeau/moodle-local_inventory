<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory;

/**
 * Description of Areas
 *
 * @author patrick
 */
class Areas {
    
    private $areas;
    
    /**
     * 
     * @global \stdCLass $CFG
     * @global \moodle_database $DB
     */
    public function __construct() {
        global $CFG, $DB;
        
        $this->areas = $DB->get_records('local_inventory_area',[]);
        
    }
    
    public function get_areas() {
        return $this->areas;
    }


}
