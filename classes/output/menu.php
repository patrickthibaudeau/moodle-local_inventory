<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory\output;

/**
 * 
 * @global \stdClass $USER
 * @param \renderer_base $output
 * @return array
 */
class menu implements \renderable, \templatable {

    

    public function __construct() {
        
        
    }

    /**
     * 
     * @global \stdClass $USER
     * @global \moodle_database $DB
     * @param \renderer_base $output
     * @return array
     */
    public function export_for_template(\renderer_base $output) {
        global $CFG, $USER, $DB;
        
        $storesObj = new \local_inventory\Stores();
        $stores = $storesObj->get_stores();
        
        $storesArray = [];
        $i = 0;
        foreach ($stores as $s) {
            $storesArray[$i]['id'] = $s->id;
            $storesArray[$i]['name'] = $s->name;
            $i++;
        }
       
        $data = [
            'wwwroot' => $CFG->wwwroot,
            'stores' => $storesArray
        ];       

        return $data;
    }
}


