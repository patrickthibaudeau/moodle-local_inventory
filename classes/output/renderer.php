<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace local_inventory\output;
/**
 * Description of renderer
 *
 * @author patrick
 */
class renderer extends \plugin_renderer_base {
    
    public function render_menu(\templatable $menu) {
        $data = $menu->export_for_template($this);
        return $this->render_from_template('local_inventory/menu', $data);
    }
    
    public function render_areas(\templatable $areas) {
        $data = $areas->export_for_template($this);
        return $this->render_from_template('local_inventory/areas', $data);
    }
    
}

