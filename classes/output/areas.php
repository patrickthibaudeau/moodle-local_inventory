<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory\output;

/**
 * 
 * @global \stdClass $USER
 * @param \renderer_base $output
 * @return array
 */
class areas implements \renderable, \templatable {

    

    public function __construct() {
        
        
    }

    /**
     * 
     * @global \stdClass $USER
     * @global \moodle_database $DB
     * @param \renderer_base $output
     * @return array
     */
    public function export_for_template(\renderer_base $output) {
        global $CFG, $USER, $DB;
        
        $areasObj = new \local_inventory\Areas();
        $areas = $areasObj->get_areas();
       
        $areasArray = [];
        $i = 0;
        foreach ($areas as $a) {
            $areasArray[$i]['id'] = $a->id;
            $areasArray[$i]['name'] = $a->name;
            $i++;
        }
        $data = [
            'wwwroot' => $CFG->wwwroot,
            'areas' => $areasArray
        ];

        return $data;
    }
}


