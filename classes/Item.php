<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory;

/**
 * Description of Item
 *
 * @author patrick
 */
class Item {
    /*
     * Item id
     */

    private $id;

    /*
     * Item name
     */
    private $name;

    /*
     * Item name
     */
    private $sku;

    /*
     * Shelves
     */
    private $shelves;

    /*
     * Stock level
     */
    private $stock_level;

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @param int $id
     */
    public function __construct($id = 0) {
        global $CFG, $DB;
        if ($id) {

            $result = $DB->get_record('local_inventory_item', ['id' => $id]);

            $shelfSql = "SELECT
                        ss.id,
                        ss.stocklevel,
                        s.name as shelf_name,
                        s.shortname,
                        ai.name AS aisle_name,
                        ar.name AS area_name
                      FROM
                        mdl_local_inventory_item_shelf ss
                        INNER JOIN mdl_local_inventory_shelf s ON s.id = ss.shelfid
                        INNER JOIN mdl_local_inventory_aisle ai ON ai.id = s.aisleid
                        INNER JOIN mdl_local_inventory_area ar ON ar.id = ai.areaid
                      WHERE ss.itemid = ?";
            $shelves = $DB->get_records_sql($shelfSql, ['id' => $id]);

            $stockLevelSql = 'SELECT SUM(stocklevel) as stocklevel FROM {local_inventory_item_shelf}
                WHERE itemid=?';
            $stockLevel = $DB->get_record_sql($stockLevelSql, [$id]);

            $this->id = $result->id;
            $this->name = $result->name;
            $this->sku = $result->sku;
            $this->shelves = $shelves;
            $this->stock_level = $stockLevel->stocklevel;
        } else {
            $this->id = $id;
            $this->name = '';
            $this->sku = '';
            $this->shelves = new \stdClass();
        }
    }

    public function get_area_name() {
        return $this->area_name;
    }

    public function get_aisle_name() {
        return $this->aisle_name;
    }

    public function get_shelf_name() {
        return $this->shelf_name;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_name() {
        return $this->name;
    }

    public function get_sku() {
        return $this->sku;
    }

    public function get_item_info() {
        $html = '<div class="alert alert-info mb-3">'
                . '<h2>'
                . $this->name . ' (' . get_string('sku', 'local_inventory')
                . ': ' . $this->sku . ') ' . get_string('total_stocklevel', 'local_inventory')
                . ': <span id="stockLevel">' . $this->stock_level . '</span>'
                . '</h2>'
                . '</div>';
        foreach ($this->shelves as $shelf) {
            $html .= '<div class="card mb-3">';
            $html .= '  <div class="card-header card-header-primary">';
            $html .= '      <h3>' . $shelf->area_name . ' - ' . $shelf->aisle_name . '</h3>';
            $html .= '  </div>';
            $html .= '  <div class="card-body">';
            $html .= '<ul class="list-group">';
            $html .= '<li class="list-group-item">';
            $html .= get_string('shelf', 'local_inventory') . ': ' . $shelf->shelf_name;
            $html .= '</li>';
            $html .= '<li class="list-group-item">';
            $html .= get_string('stocklevel', 'local_inventory') . ': <span id="itemStockLevel-' . $shelf->id . '">' . $shelf->stocklevel . '</span>';
            if ($shelf->stocklevel > 0) {
                $html .= '  <div class="mb-3">';
                $html .= '      <button type="button" class="removeStock btn btn-sm btn-danger" data-amount="1" data-itemshelfid="' . $shelf->id . '">-1</button> ';
                $html .= '      <button type="button" class="removeStock btn btn-sm btn-danger" data-amount="5" data-itemshelfid="' . $shelf->id . '">-5</button> ';
                $html .= '      <button type="button" class="removeStock btn btn-sm btn-danger" data-amount="10" data-itemshelfid="' . $shelf->id . '">-10</button> ';
                $html .= '  </div>';
            }
            $html .= '  <div class="mb-3">';
            $html .= '      <button type="button" class="addStock btn btn-sm btn-success" data-amount="1" data-itemshelfid="' . $shelf->id . '">+1</button> ';
            $html .= '      <button type="button" class="addStock btn btn-sm btn-success" data-amount="5" data-itemshelfid="' . $shelf->id . '">+5</button> ';
            $html .= '      <button type="button" class="addStock btn btn-sm btn-success" data-amount="10" data-itemshelfid="' . $shelf->id . '">+10</button> ';
            $html .= '  </div>';
            $html .= '</li>';
            $html .= '</ul>';
            $html .= '  </div>';
            $html .= '</div>';
        }
        return $html;
    }

}
