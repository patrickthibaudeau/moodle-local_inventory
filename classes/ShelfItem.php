<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_inventory;

/**
 * Description of ShelfItem
 *
 * @author patrick
 */
class ShelfItem {

    /**
     *
     * @var int 
     */
    private $id;

    /**
     *
     * @var int 
     */
    private $item_id;

    /**
     *
     * @var int 
     */
    private $shelf_id;

    /**
     *
     * @var int 
     */
    private $stock_level;

    /**
     *
     * @var int 
     */
    private $time_created;

    /**
     *
     * @var int 
     */
    private $time_modified;
    
    /**
     *
     * @var string 
     */
    private $table;

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @param int $id
     */
    public function __construct($id = 0) {
        global $CFG, $DB;
        
        $this->table = 'local_inventory_item_shelf';

        if ($id) {
            $shelfItem = $DB->get_record($this->table, ['id' => $id]);

            $this->id = $id;
            $this->item_id = $shelfItem->itemid;
            $this->shelf_id = $shelfItem->shelfid;
            $this->stock_level = $shelfItem->stocklevel;
            $this->time_created = date('m-d-Y H:i:s', $shelfItem->timecreated);
            $this->time_modified = date('m-d-Y H:i:s', $shelfItem->timemodified);
        } else {
            $this->id = $id;
            $this->item_id = 0;
            $this->shelf_id = 0;
            $this->stock_level = 0;
            $this->time_created = 0;
            $this->time_modified = 0;
        }
    }
    
    public function get_id() {
        return $this->id;
    }

    public function get_item_id() {
        return $this->item_id;
    }

    public function get_shelf_id() {
        return $this->shelf_id;
    }

    public function get_stock_level() {
        return $this->stock_level;
    }

    public function get_time_created() {
        return $this->time_created;
    }

    public function get_time_modified() {
        return $this->time_modified;
    }

    /**
     * 
     * @global \moodle_database $DB
     * @param array $data
     */
    public function update($data) {
        global $DB, $USER;
        
        if (!isset($data['id'])) {
            return false;
        }
        
        $data['timemodified'] = time();
        
        if ($DB->update_record($this->table, $data)) {
            return true;
        }        
        return false;        
    }
    
    /**
     * 
     * @global \moodle_database $DB
     * @param array $data
     */
    public function insert($data) {
        global $DB, $USER;
        
        $data['timecreated'] = time();
        $data['timemodified'] = time();
        
        if ($DB->insert_record($this->table, $data)) {
            return true;
        }        
        return false;        
    }

}
