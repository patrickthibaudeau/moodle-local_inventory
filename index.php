<?php

/**
 * *************************************************************************
 * *                             inventory                                **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  inventory                                                 **
 * @name        inventory                                                 **
 * @copyright   Dominik Thibaudeau                                        **
 * @link                                                                  **
 * @author      Dominik Thibaudeau                                        **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once('config.php');

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $COURSE, $USER;

    $id = optional_param('id', 0, PARAM_INT); //List id

    require_login(1, false); //Use course 1 because this has nothing to do with an actual course, just like course 1

    $context = context_system::instance();

    $pagetitle = get_string('pluginname', 'local_inventory');
    $pageheading = get_string('pluginname', 'local_inventory');

    echo \local_inventory\Base::page($CFG->wwwroot . '/local/inventory/index.php', $pagetitle, $pageheading, $context);


    $HTMLcontent = '';
    //**********************
    //*** DISPLAY HEADER ***
    //**********************
    echo $OUTPUT->header();
    //**********************
    //*** DISPLAY CONTENT **
    //**********************
    $output = $PAGE->get_renderer('local_inventory');
    $menu = new \local_inventory\output\menu();

//    $PAGE->requires->js_call_amd('local_inventory/menu', 'init');

    echo $output->render_menu($menu);
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();
?>
