<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin upgrade steps are defined here.
 *
 * @package     local_inventory
 * @category    upgrade
 * @copyright   2018 Dominik Thibaudeau<thibaud@yorku.ca>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Execute local_inventory upgrade from the given old version.
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_local_inventory_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2018111802) {
        // Rename field areaid on table local_inventory_shelf to NEWNAMEGOESHERE.
        $table = new xmldb_table('local_inventory_shelf');
        $field = new xmldb_field('areaid', XMLDB_TYPE_INTEGER, '20', null, null, null, '0', 'id');

        // Launch rename field areaid.
        $dbman->rename_field($table, $field, 'ailseid');

        // Define table local_inventory_aisle to be created.
        $table = new xmldb_table('local_inventory_aisle');

        // Adding fields to table local_inventory_aisle.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('areaid', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');

        // Adding keys to table local_inventory_aisle.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_inventory_aisle.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }


        // Inventory savepoint reached.
        upgrade_plugin_savepoint(true, 2018111802, 'local', 'inventory');
    }

    if ($oldversion < 2018112300) {

        // Define field location to be added to local_inventory_shelf.
        $table = new xmldb_table('local_inventory_shelf');
        $field = new xmldb_field('location', XMLDB_TYPE_CHAR, '1', null, null, null, null, 'shortname');

        // Conditionally launch add field location.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Inventory savepoint reached.
        upgrade_plugin_savepoint(true, 2018112300, 'local', 'inventory');
    }

    if ($oldversion < 2018112301) {

        // Define table local_inventory_store to be created.
        $table = new xmldb_table('local_inventory_store');

        // Adding fields to table local_inventory_store.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('address', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '1333', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');

        // Adding keys to table local_inventory_store.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_inventory_store.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define field storeid to be added to local_inventory_area.
        $table = new xmldb_table('local_inventory_area');
        $field = new xmldb_field('storeid', XMLDB_TYPE_INTEGER, '20', null, null, null, '0', 'id');

        // Conditionally launch add field storeid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Inventory savepoint reached.
        upgrade_plugin_savepoint(true, 2018112301, 'local', 'inventory');
    }

    if ($oldversion < 2018112400) {

        // Define field shelfid to be dropped from local_inventory_item.
        $table = new xmldb_table('local_inventory_item');
        $field = new xmldb_field('shelfid');

        // Conditionally launch drop field shelfid.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define table local_inventory_item_shelf to be created.
        $table = new xmldb_table('local_inventory_item_shelf');

        // Adding fields to table local_inventory_item_shelf.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('itemid', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');
        $table->add_field('shelfid', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('stocklevel', XMLDB_TYPE_INTEGER, '6', null, null, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '20', null, null, null, '0');

        // Adding keys to table local_inventory_item_shelf.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_inventory_item_shelf.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Inventory savepoint reached.
        upgrade_plugin_savepoint(true, 2018112400, 'local', 'inventory');
    }


    return true;
}
