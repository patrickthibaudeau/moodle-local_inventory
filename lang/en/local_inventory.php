<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_inventory
 * @category    string
 * @copyright   2018 Dominik Thibaudeau<thibaud@yorku.ca>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aisle'] = 'Aisle';
$string['area'] = 'Area';
$string['areas'] = 'Areas';
$string['create'] = 'Create';
$string['enter_sku'] = 'Enter SKU';
$string['home'] = 'Home';
$string['item'] = 'Item';
$string['name'] = 'Name';
$string['pluginname'] = 'Toys R Us Inventory system';
$string['search'] = 'Search';
$string['shelf'] = 'Shelf';
$string['sku'] = 'SKU';
$string['stocklevel'] = 'Stock level';
$string['total_stocklevel'] = 'Total Stock level';
