<?php

/**
 * *************************************************************************
 * *                             exchange                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  exchange                                                  **
 * @name        exchange                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once('config.php');
include("area_form.php");

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;
    $COURSE;

    require_login(1, FALSE);
    $id = optional_param('id',0, PARAM_INT);

    //Set principal parameters
    $context = CONTEXT_SYSTEM::instance();

    if ($id) {
        $formdata = $DB->get_record('local_inventory_area', array('id' => $id), '*', MUST_EXIST);
    } else {
        $formdata = new stdClass();
    }

    $mform = new inventory_area_form(null, array('formdata' => $formdata));

// If data submitted, then process and store.
    if ($mform->is_cancelled()) {
        redirect($CFG->wwwroot . '/local/inventory/areas.php');
    } else if ($data = $mform->get_data()) {

        $data->timemodified = time();

        if ($data->id) {            
            //update record
            $DB->update_record('local_inventory_area', $data);
        } else {
            $data->timecreated = time();
            $DB->insert_record('local_inventory_area', $data);
        }

        redirect($CFG->wwwroot . '/local/inventory/areas.php');
    }



    echo \local_inventory\Base::page($CFG->wwwroot . '/local/inventory/area.php?id=' . $id, get_string('pluginname', 'local_inventory'), get_string('area', 'local_inventory'), $context);
    //--------------------------------------------------------------------------
    echo $OUTPUT->header();
    //**********************
    $mform->display();

    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();
?>
