<?php
/**
 * *************************************************************************
 * *                             inventory                                **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  inventory                                                 **
 * @name        inventory                                                 **
 * @copyright   Dominik Thibaudeau                                        **
 * @link                                                                  **
 * @author      Dominik Thibaudeau                                        **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(__FILE__) . '../../../config.php');
//require_once('locallib.php');


require_once($CFG->dirroot . '/local/inventory/classes/Areas.php');
//require_once($CFG->dirroot . '/local/inventory/classes/Area.php');
require_once($CFG->dirroot . '/local/inventory/classes/Base.php');
require_once($CFG->dirroot . '/local/inventory/classes/Item.php');
require_once($CFG->dirroot . '/local/inventory/classes/ShelfItem.php');
require_once($CFG->dirroot . '/local/inventory/classes/Stores.php');
