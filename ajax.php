<?php
require_once('config.php');

global $CFG, $USER, $DB;
$action = required_param('action', PARAM_TEXT);

switch ($action) {
    case 'searchSku':
        echo \local_inventory\Base::searchSku();
        break;
    case 'getItem':
        $id = required_param('id', PARAM_INT);
        $ITEM = new \local_inventory\Item($id);
        echo $ITEM->get_item_info();
        break;
    case 'addStock':
        $id = required_param('id', PARAM_INT);
        $amount = required_param('amount', PARAM_INT);
        $SHELFITEM = new \local_inventory\ShelfItem($id);
        
        $newAmount = $SHELFITEM->get_stockLevel() + $amount;
        $data = [
            'id' => $id,
            'stocklevel' => $newAmount,
        ];
        $SHELFITEM->update($data);
        
        $ITEM = new \local_inventory\Item($SHELFITEM->get_itemId());
        echo $ITEM->get_item_info();
        break;
    case 'removeStock':
        $id = required_param('id', PARAM_INT);
        $amount = required_param('amount', PARAM_INT);
        $SHELFITEM = new \local_inventory\ShelfItem($id);
        
        $newAmount = $SHELFITEM->get_stockLevel() - $amount;
        
        //Must never go below 0
        if ($newAmount < 0) {
            $newAmount = 0;
        }
        $data = [
            'id' => $id,
            'stocklevel' => $newAmount,
        ];
        $SHELFITEM->update($data);
        
        $ITEM = new \local_inventory\Item($SHELFITEM->get_itemId());
        echo $ITEM->get_item_info();
        break;
    case 'deleteArea':
        $id = required_param('id', PARAM_INT);
        
        break;
}


